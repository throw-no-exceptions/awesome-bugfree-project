/****************************************************************************
**
** Copyright (C) 2017 Klarälvdalens Datakonsult AB, a KDAB Group company, info@kdab.com, author Milian Wolff <milian.wolff@kdab.com>
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtWebChannel module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dialog.h"
#include "ui_dialog.h"
#include <QProcess>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    connect(ui->send, &QPushButton::clicked, this, &Dialog::clicked);

    banana = false;
}

void Dialog::displayMessage(const QString &message)
{
    ui->output->appendPlainText(message);
}

void Dialog::clicked()
{
    const QString text = ui->input->text();

    if (text.isEmpty())
        return;

    emit sendText(text);
    displayMessage(tr("Sent message: %1").arg(text));

    ui->input->clear();
}

void Dialog::on_insertImageButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "C:\\Users\\phili\\Desktop\\qtChat\\images",
                                                    tr("Text (*.txt)"));

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);

    QString line;

    while(!in.atEnd()) {
        line = in.readLine();
        QStringList fields = line.split(",");
    }

    file.close();

    QProcess process;

    QString scriptFile =  "C:\\Users\\phili\\Desktop\\qtChat\\find_fruit.py";

    QStringList pythonCommandArguments = {scriptFile, line};

    process.start ("C:\\Users\\phili\\AppData\\Local\\Programs\\Python\\Python37\\python.exe", pythonCommandArguments);

    process.waitForFinished();

    process.waitForReadyRead();

    QString word = process.readAll();

    word.remove(QRegExp("[\\n\\t\\r]"));

    qDebug() << word;

    if(word == "banana") {
        // if it is a banana pic
        // send a blurred version of the pic

        emit sendText("image bad " + line);

    }

    else {
        // if it is NOT a banana pic
        emit sendText("image safe " + line);
    }

}
