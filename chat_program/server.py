# from socket import AF_INET, socket, SOCK_STREAM
from socket import *
from threading import Thread
from common import *

clients = {}
addresses = {}

SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDRESS)

def accept_incoming_connections():
    while True:
        (client, client_address) = SERVER.accept()
        print("client address is: ", client_address)
        print("%s:%s has connected." % client_address)
        client.send("name: ".encode("utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,)).start()

def handle_client(client):
    name = client.recv(BUFF_SIZE).decode("utf8")
    welcome_msg = f"{name}. type /quit to exit."
    print(f"{name} has joined")
    client.send(welcome_msg.encode("utf8"))
    msg = f"{name} has joined"
    broadcast(msg)
    clients[client] = name
    while True:
        msg = client.recv(BUFF_SIZE).decode("utf-8").strip()
        if msg == "/quit":
            client.close()
            del clients[client]
            broadcast(f"{name} has left")
            break
        elif is_url(msg):
            handle_url(msg, name)
        else:
            broadcast(msg, name+": ")

def broadcast(msg, prefix=""):
    for sock in clients:
        sock.send((prefix + msg).encode("utf8"))

def handle_url(url, name):
    broadcast(url, name+": ")

if __name__ == "__main__":
    SERVER.listen(5)
    print("Waiting for connections")
    try:
        accept_incoming_connections()
    except KeyboardInterrupt:
        print("interrupted, cleaning up...")
    finally:
        SERVER.close()
        print("all clean")
