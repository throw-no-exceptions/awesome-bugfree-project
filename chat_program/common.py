import requests
import json
from urllib.parse import urlparse

phallic_fruit = "banana"
api_key = "ca90013b71ee4494823bea16f8ed36d7"
api_url = "https://api.clarifai.com/v2/models/aaa03c23b3724a16a56b629203edc62c/outputs"

HOST = ""
PORT = 4242
BUFF_SIZE = 1024
ADDRESS = (HOST, PORT)

def find_fruit(fruit, image_url):
    data_dict = json.dumps({ "inputs": [ { "data": { "image": { "url": image_url } } } ] })
    headers_dict = {"Authorization": f"Key {api_key}", "Content-Type": "application/json"}

    response = requests.request("POST", api_url, data=data_dict, headers=headers_dict)

    if response.status_code != 200:
#         raise RuntimeError(f"status code {response.status_code}")
        return False

    response = json.loads(response.text)

    status = response.get("status").get("description")

    if status != "Ok":
#         raise RuntimeError(f"error: status {status}")
        return False

    concept_dicts = response.get("outputs")[0].get("data").get("concepts")
    concept_dicts = sorted(concept_dicts, reverse=True, key=(lambda x: x.get("value")))

    for concept in concept_dicts:
#         print("{: <16}: {: >16}".format(concept.get("name"), concept.get('value')))
        if concept.get("name") == "banana" and concept.get("value") > 0.5:
            return True
    else:
        return False

def is_url(string):
    try:
        result = urlparse(string)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False

def get_urls(string):
    for substr in string.split(" "):
        if is_url(substr):
            yield substr
    return []
