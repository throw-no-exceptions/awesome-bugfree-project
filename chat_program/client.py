from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import sys
from common import *

def receive_message():
    while True:
        try:
            msg = client_socket.recv(BUFF_SIZE).decode("utf8")
            if msg.split(":", 1)[0] == name:
                continue
            for url in get_urls(msg):
                if find_fruit("banana", url):
                    print(f"warning: msg might contain a naughty picture: {url}")
            print(msg)
        except OSError:
            break

def send(msg):
    client_socket.send(bytes(msg, "utf8"))
    if msg == "/quit":
        client_socket.close()
        sys.exit(0)

if len(sys.argv) > 2:
    try:
        (HOST, PORT) = sys.argv.split(":")
        PORT = int(PORT)
    except ValueError:
        print("invalid argument, try again.")
        sys.exit(1)

name = ""

if __name__ == "__main__":
    client_socket = socket(AF_INET, SOCK_STREAM)
    client_socket.connect(ADDRESS)

    name_msg = client_socket.recv(BUFF_SIZE).decode("utf8")
    name = input("what is your name? ")
    send(name)

    receive_thread = Thread(target=receive_message)
    receive_thread.start()

    while True:
        msg = input()
        send(msg)
