#!/usr/bin/python3

phallic_fruit = "banana"
api_key = "ca90013b71ee4494823bea16f8ed36d7"
api_url = "https://api.clarifai.com/v2/models/aaa03c23b3724a16a56b629203edc62c/outputs"

import requests
import sys
import json
import pprint

def find_fruit(fruit, image_url):
    data_dict = json.dumps({ "inputs": [ { "data": { "image": { "url": image_url } } } ] })
    headers_dict = {"Authorization": f"Key {api_key}", "Content-Type": "application/json"}

    response = requests.request("POST", api_url, data=data_dict, headers=headers_dict)

    if response.status_code != 200:
        raise RuntimeError(f"status code {response.status_code}")

    response = json.loads(response.text)

    status = response.get("status").get("description")

    if status != "Ok":
        raise RuntimeError(f"error: status {status}")

    concept_dicts = response.get("outputs")[0].get("data").get("concepts")
    concept_dicts = sorted(concept_dicts, reverse=True, key=(lambda x: x.get("value")))

    for concept in concept_dicts:
#         print("{: <16}: {: >16}".format(concept.get("name"), concept.get('value')))
        if concept.get("name") == "banana" and concept.get("value") > 0.5:
            return True
    else:
        return False

def main():
    if len(sys.argv) < 2:
        image_url = "https://upload.wikimedia.org/wikipedia/commons/f/ff/Banana_and_cross_section.jpg"
    else:
        image_url = sys.argv[1]

    if find_fruit(phallic_fruit, image_url):
        print("there's always money in the banana stand")
    else:
        print("no banana")

main()
